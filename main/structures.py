class Category(object):
    category_code = 0
    name_of_category = ""

    def __init__(self, data):
        self.category_code = data['category_code']
        self.name_of_category = data['name_of_category']


class Manufacturer(object):
    manufacturer = ""
    country = ""

    def __init__(self, data):
        self.manufacturer = data['manufacturer']
        self.country = data['country']


class StoragePlace(object):
    building = 0
    room = 0

    def __init__(self, data):
        self.building = data['building']
        self.room = data['room']


class Condition(object):
    works = False
    location = ""

    def __init__(self, data):
        self.works = data['works']
        self.location = data['location']

    # def __str__(self):
    #     return f"Condition: {self.works}"


class Description(object):

    def __init__(self, data):
        self.id = data['id']
        self.device_name = data['device_name']
        self.brand = data['brand']
        self.model = data['model']
        self.manufacturer = Manufacturer(data['manufacturer'])
        self.category = Category(data['category'])


class Report(object):
    id = 0
    serial_number = 0
    inventory_number = 0
    manufacture_date = 0

    def __init__(self, data):
        self.id = data['id']
        self.description = Description(data['description'])
        self.serial = data['serial']
        self.inventory = data['inventory']
        self.date = data['date']
        self.storage = StoragePlace(data['storage'])
        self.condition = Condition(data['condition'])

    # def __str__(self):
    #     return self.serial
