from django.db import models


class Manufacturer(models.Model):
    name = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    desc = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class EquipmentType(models.Model):
    name = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    manufacturer = models.ForeignKey(Manufacturer,on_delete=models.CASCADE,default=0)
    category = models.ForeignKey(Category,on_delete=models.CASCADE,default=0)
    desc = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Condition(models.Model):
    name = models.CharField(max_length=100)
    works = models.BooleanField()
    desc = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Building(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    desc = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Storage(models.Model):
    name = models.CharField(max_length=100)
    building = models.ForeignKey(Building,on_delete=models.CASCADE,default=0)
    room = models.IntegerField()
    desc = models.TextField(null=True, blank=True)


class EquipmentComplex(models.Model):
    name = models.CharField(max_length=100)
    list = models.ManyToManyField(EquipmentType, through='Membership')

    def __str__(self):
        return self.name


class Membership(models.Model):
    name = models.CharField(max_length=100)
    equipment = models.ForeignKey(EquipmentType, on_delete=models.CASCADE)
    complex = models.ForeignKey(EquipmentComplex, on_delete=models.CASCADE)
