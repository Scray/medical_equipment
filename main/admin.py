from django.contrib import admin
from .models import Manufacturer
from .models import Category
from .models import Storage
from .models import Condition
from .models import Building
from .models import EquipmentType

admin.site.register(Manufacturer)
admin.site.register(Category)
admin.site.register(Storage)
admin.site.register(Condition)
admin.site.register(Building)
admin.site.register(EquipmentType)