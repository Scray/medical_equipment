import json
from os import listdir, path

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .forms import *
from django.db.models import DEFERRED
from .models import *


def index(request):
    return render(request, 'main/index.html')


def manufacturers(request):
    manufacturer_list = Manufacturer.objects.all()
    return render(request, 'main/manufacturers.html', {'manufacturer_list': manufacturer_list})


def manufacturer_edit(request, pk):
    man = get_object_or_404(Manufacturer, pk=pk)
    if request.method == "POST":
        form = ManufacturerForm(request.POST, instance=man)
        if form.is_valid():
            manufacturer = form.save(commit=False)
            manufacturer.save()
            return redirect('manufacturers')
    else:
        form = ManufacturerForm(instance=man)
    return render(request, 'main/manufacturer_edit.html', {'form': form})


def manufacturer_new(request):
    if request.method == "POST":
        form = ManufacturerForm(request.POST)
        if form.is_valid():
            manufacturer = form.save(commit=False)
            manufacturer.save()
            return redirect('manufacturers')
    else:
        form = ManufacturerForm()
    return render(request, 'main/manufacturer_edit.html', {'form': form})


def categories(request):
    category_list = Category.objects.all()
    return render(request, 'main/categories.html', {'category_list': category_list})


def category_edit(request, pk):
    cat = get_object_or_404(Category, pk=pk)
    if request.method == "POST":
        form = CategoryForm(request.POST, instance=cat)
        if form.is_valid():
            category = form.save(commit=False)
            category.save()
            return redirect('categories')
    else:
        form = CategoryForm(instance=cat)
    return render(request, 'main/category_edit.html', {'form': form})


def category_new(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.save()
            return redirect('categories')
    else:
        form = CategoryForm()
    return render(request, 'main/category_edit.html', {'form': form})


def storage(request):
    storage_list = Storage.objects.all()
    return render(request, 'main/storage.html', {'storage_list': storage_list})


def storage_edit(request, pk):
    cat = get_object_or_404(Storage, pk=pk)
    if request.method == "POST":
        form = StorageForm(request.POST, instance=cat)
        if form.is_valid():
            storage = form.save(commit=False)
            storage.save()
            return redirect('storage')
    else:
        form = StorageForm(instance=cat)
    return render(request, 'main/storage_edit.html', {'form': form})


def storage_new(request):
    if request.method == "POST":
        form = StorageForm(request.POST)
        if form.is_valid():
            storage = form.save(commit=False)
            storage.save()
            return redirect('storage')
    else:
        form = StorageForm()
    return render(request, 'main/storage_edit.html', {'form': form})


def conditions(request):
    conditions_list = Condition.objects.all()
    return render(request, 'main/conditions.html', {'conditions_list': conditions_list})


def conditions_edit(request, pk):
    cat = get_object_or_404(Storage, pk=pk)
    if request.method == "POST":
        form = ConditionForm(request.POST, instance=cat)
        if form.is_valid():
            condition = form.save(commit=False)
            condition.save()
            return redirect('conditions')
    else:
        form = ConditionForm(instance=cat)
    return render(request, 'main/conditions_edit.html', {'form': form})


def conditions_new(request):
    if request.method == "POST":
        form = ConditionForm(request.POST)
        if form.is_valid():
            condition = form.save(commit=False)
            condition.save()
            return redirect('conditions')
    else:
        form = ConditionForm()
    return render(request, 'main/conditions_edit.html', {'form': form})


def buildings(request):
    buildings_list = Building.objects.all()
    return render(request, 'main/buildings.html', {'buildings_list': buildings_list})


def buildings_edit(request, pk):
    cat = get_object_or_404(Storage, pk=pk)
    if request.method == "POST":
        form = BuildingForm(request.POST, instance=cat)
        if form.is_valid():
            building = form.save(commit=False)
            building.save()
            return redirect('buildings')
    else:
        form = BuildingForm(instance=cat)
    return render(request, 'main/buildings_edit.html', {'form': form})


def buildings_new(request):
    if request.method == "POST":
        form = BuildingForm(request.POST)
        if form.is_valid():
            building = form.save(commit=False)
            building.save()
            return redirect('buildings')
    else:
        form = BuildingForm()
    return render(request, 'main/buildings_edit.html', {'form': form})


def equipment_types(request):
    equipment_types_list = EquipmentType.objects.all()
    return render(request, 'main/equipment_types.html', {'equipment_types_list': equipment_types_list})


def equipment_type_edit(request, pk):
    cat = get_object_or_404(EquipmentType, pk=pk)
    if request.method == "POST":
        form = EquipmentTypeForm(request.POST, instance=cat)
        if form.is_valid():
            equipment_type = form.save(commit=False)
            equipment_type.save()
            return redirect('equipment_types')
    else:
        form = EquipmentTypeForm(instance=cat)
    return render(request, 'main/equipment_type_edit.html', {'form': form})


def equipment_type_new(request):
    if request.method == "POST":
        form = EquipmentTypeForm(request.POST)
        if form.is_valid():
            equipment_type = form.save(commit=False)
            equipment_type.save()
            return redirect('equipment_types')
    else:
        form = EquipmentTypeForm()
    return render(request, 'main/equipment_type_edit.html', {'form': form})


def about():
    return HttpResponse("<h4>Information about author</h4>")
