from django.forms import ModelForm
from django import forms
from .models import *


class ManufacturerForm(ModelForm):
    class Meta:
        model = Manufacturer
        fields = ['name', 'country', 'desc']
        labels = {
            'name': ('Название'),
            'country': ('Страна'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class EquipmentForm(ModelForm):
    class Meta:
        model = Manufacturer
        fields = ['name', 'country', 'desc']
        labels = {
            'name': ('Название'),
            'country': ('Страна'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'desc']
        labels = {
            'name': ('Название'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class ConditionForm(ModelForm):
    class Meta:
        model = Condition
        fields = ['name', 'works', 'desc']
        labels = {
            'name': ('Название'),
            'works': ('Работоспособность'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class EquipmentTypeForm(ModelForm):
    class Meta:
        model = EquipmentType
        fields = ['name', 'model', 'manufacturer', 'category', 'desc']
        labels = {
            'name': ('Название'),
            'model': ('Модель'),
            'manufacturer': ('Производитель'),
            'category': ('Категория'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class BuildingForm(ModelForm):
    class Meta:
        model = Building
        fields = ['name', 'address', 'desc']
        labels = {
            'name': ('Название'),
            'address': ('Адрес'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class StorageForm(ModelForm):
    class Meta:
        model = Storage
        fields = ['name', 'room', 'building', 'desc']
        labels = {
            'name': ('Название'),
            'building': ('Здание'),
            'room': ('Кабинет'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }


class Equipment(ModelForm):
    class Meta:
        model = EquipmentType
        fields = ['name', 'model', 'manufacturer', 'category', 'desc']
        labels = {
            'name': ('Название'),
            'model': ('Модель'),
            'manufacturer': ('Производитель'),
            'category': ('Категория'),
            'desc': ('Описание'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '60'}),
        }