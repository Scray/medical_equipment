from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('about', views.about, name='about'),
    path('categories', views.categories, name='categories'),
    path('category/<int:pk>/', views.category_edit, name='category_edit'),
    path('category_new', views.category_new, name='category_edit'),
    path('storage', views.storage, name='storage'),
    path('storage/<int:pk>/', views.storage_edit, name='storage_edit'),
    path('storage_new', views.storage_new, name='storage_edit'),
    path('conditions', views.conditions, name='conditions'),
    path('conditions/<int:pk>/', views.conditions_edit, name='conditions_edit'),
    path('conditions_new', views.conditions_new, name='conditions_edit'),
    path('buildings', views.buildings, name='buildings'),
    path('buildings/<int:pk>/', views.buildings_edit, name='buildings_edit'),
    path('buildings_new', views.buildings_new, name='buildings_edit'),
    path('manufacturers', views.manufacturers, name='manufacturers'),
    path('manufacturer/<int:pk>/', views.manufacturer_edit, name='manufacturer_edit'),
    path('manufacturer_new', views.manufacturer_new, name='manufacturer_edit'),
    path('equipment_types', views.equipment_types, name='equipment_types'),
    path('equipment_type/<int:pk>/', views.equipment_type_edit, name='equipment_type_edit'),
    path('equipment_type_new', views.equipment_type_new, name='equipment_type_edit'),
]
